beautifulsoup4==4.9.3
certifi==2020.12.5
chardet==4.0.0
click==7.1.2
cycler==0.10.0
gender-guesser==0.4.0
idna==2.10
joblib==1.0.0
kiwisolver==1.3.1
levenshtein==0.12.0
matplotlib==3.3.3
nltk==3.5
numpy==1.19.5
pandas==1.2.0
Pillow==8.1.0
pyparsing==2.4.7
python-dateutil==2.8.1
pytz==2020.5
regex==2020.11.13
requests==2.25.1
six==1.15.0
soupsieve==2.1
tqdm==4.55.2
Unidecode==1.1.2
urllib3==1.26.2
wordcloud==1.8.1
