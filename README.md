# Introduction

This repository holds the codes developed for supporting a research project in Social Science.
The codes were mentioned at the paper 
"Computational Mining on IBICT BDTD’s Thesis and Dissertation Metadata for Supporting Social Science Research", accepted for ENIAC 2020.

# Description

All codes are in Python 3. The repository has the following files:
 * Preprocessing -- implements all preprocessing routines necessary to create new CSV and JSON auxiliary files 
 * questionOne, questionTwo, ... -- are the main routines for producing the information visualizations
 * requirements.txt -- lists all programs and libraries for running the python codes.

 # What is the Social Science Research Project?

 This paper focuses on a research project for understanding the intersection between gender and race categories in dissertation and thesis academic publications in Brazil.
The overall goal of the project is to map the rising of that intersection over the years, its geographic distribution in the country and the main related themes discussed by academic authors according to their sex. 

# How the data was generated and collected?

The analyzed data were collected from a simple search for keywords in the Brazilian Digital Library of Theses and Dissertations repository (BDTD <https://bdtd.ibict.br/vufind/>. We used 18 keywords, from which we could point out as examples: gender, raça, raci*, femin*, mascul*, etni*. Each search generated a unique file that corresponds to the 18 CSV files worked on this paper.

# What questions were developed?

(Q1) What are the main topics of the master dissertations and doctorate thesis for each theme? How related are those topics to the academic research done in every institution and geopolitical region? 

(Q2) How are the works distributed according to their knowledge area, year of publication and institution? 

(Q3) How are the student-advisor matrices by theme, year and geopolitical region? 

# What is the relevance of this research?

Be an example of multidisciplinary collaborative work

Assist studies on gender and race in Brazil with the analysis of concepts distributed over the years, by institution, sex, language, and area of knowledge.

Create content and make it available to be used in other researches conducted at BDTD.

Support interoperability between national and international digital libraries, by developing research tools on theses and dissertations for the areas of social sciences, information, bibliometrics, scientometrics, and digital sociology. BDTD for example is part of two other international bases: La referência <http://www.lareferencia.info/pt/> and NLDT <http://www.ndltd.org/>.